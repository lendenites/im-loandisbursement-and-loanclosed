package implusloancount;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class ClosedLoanCount
{
	WebDriver driver;
	//String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6MTA2MSwiZmlsdGVyIjpbImFuZCIsWyI9IixbImZpZWxkLWlkIiwxMTcxNV0sIkNMT1NFRCJdLFsiPiIsWyJmaWVsZC1pZCIsMTE3MDFdLCIyMDIyLTA0LTEwIl1dLCJhZ2dyZWdhdGlvbiI6W1siY291bnQiXV0sImJyZWFrb3V0IjpbWyJkYXRldGltZS1maWVsZCIsWyJmaWVsZC1pZCIsMTE3MDFdLCJkYXkiXV0sIm9yZGVyLWJ5IjpbWyJhc2MiLFsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDExNzAxXSwiZGF5Il1dXX0sImRhdGFiYXNlIjo5fSwiZGlzcGxheSI6ImxpbmUiLCJ2aXN1YWxpemF0aW9uX3NldHRpbmdzIjp7InRhYmxlLnBpdm90IjpmYWxzZX19";
	//String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjo5LCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjEwNjEsImZpbHRlciI6WyJhbmQiLFsiPSIsWyJmaWVsZC1pZCIsMTE3MTVdLCJDTE9TRUQiXSxbIj4iLFsiZmllbGQtaWQiLDExNzAxXSwiMjAyMi0wMS0wMSJdXSwiYWdncmVnYXRpb24iOltbImNvdW50Il1dLCJicmVha291dCI6W1siZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDExNzAxXSwiZGF5Il1dLCJvcmRlci1ieSI6W1siYXNjIixbImRhdGV0aW1lLWZpZWxkIixbImZpZWxkLWlkIiwxMTcwMV0sImRheSJdXV19LCJ0eXBlIjoicXVlcnkifSwiZGlzcGxheSI6InRhYmxlIiwidmlzdWFsaXphdGlvbl9zZXR0aW5ncyI6eyJ0YWJsZS5waXZvdCI6ZmFsc2V9fQ==";
	String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6MTA2MSwiZmlsdGVyIjpbImFuZCIsWyI9IixbImZpZWxkLWlkIiwxMTcxNV0sIkNMT1NFRCJdLFsiPiIsWyJmaWVsZC1pZCIsMTE3MDFdLCIyMDIyLTA0LTEwIl1dLCJhZ2dyZWdhdGlvbiI6W1siY291bnQiXV0sImJyZWFrb3V0IjpbWyJkYXRldGltZS1maWVsZCIsWyJmaWVsZC1pZCIsMTE3MDFdLCJkYXkiXV0sIm9yZGVyLWJ5IjpbWyJkZXNjIixbImRhdGV0aW1lLWZpZWxkIixbImZpZWxkLWlkIiwxMTcwMV0sImRheSJdXV19LCJkYXRhYmFzZSI6OX0sImRpc3BsYXkiOiJ0YWJsZSIsInZpc3VhbGl6YXRpb25fc2V0dGluZ3MiOnsidGFibGUucGl2b3QiOmZhbHNlfX0=";
	public ClosedLoanCount(WebDriver driver) throws InterruptedException, IOException
	{
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		Thread.sleep(3000);
		driver.get(Url);
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[2]/div/div[2]/div/div[1]")).click();
		Thread.sleep(2000);
		

		//		try {
		//			JavascriptExecutor js = (JavascriptExecutor) driver;                
		//			WebElement Element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[55]/div"));
		//			((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", Element);
		//} catch(Exception e) {}
		try {
			
			SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
			Date date = new Date();
			String fileName = df.format(date);
			File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file,
					new File("./ImPlusLoanCount/" + "ClosedLoanCount" + " " + "(" + " " + fileName + " " + ")" + ".jpg"));
		} catch (Exception e) {
		}   
		Thread.sleep(2000);

	}

}
