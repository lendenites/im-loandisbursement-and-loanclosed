package implusloancount;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DisbursedLoanCount
{
	WebDriver driver;
	//String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6MTA2MSwiZmlsdGVyIjpbImFuZCIsWyI9IixbImZpZWxkLWlkIiwxMTcxNV0sIkRJU0JVUlNFRCJdLFsiPSIsWyJmaWVsZC1pZCIsMTE3MTFdLCIxIl1dLCJhZ2dyZWdhdGlvbiI6W1siY291bnQiXV0sImJyZWFrb3V0IjpbWyJkYXRldGltZS1maWVsZCIsWyJmaWVsZC1pZCIsMTE3MDFdLCJkYXkiXV19LCJkYXRhYmFzZSI6OX0sImRpc3BsYXkiOiJzY2FsYXIiLCJ2aXN1YWxpemF0aW9uX3NldHRpbmdzIjp7ImdyYXBoLmxhYmVsX3ZhbHVlX2ZyZXF1ZW5jeSI6ImFsbCJ9fQ==";
	//String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6MTA2MSwiZmlsdGVyIjpbImFuZCIsWyI9IixbImZpZWxkLWlkIiwxMTcxNV0sIkRJU0JVUlNFRCJdLFsiPSIsWyJmaWVsZC1pZCIsMTE3MTFdLCIxIl0sWyI-IixbImZpZWxkLWlkIiwxMTcwM10sIjIwMjItMDQtMTAiXV0sImFnZ3JlZ2F0aW9uIjpbWyJjb3VudCJdXSwiYnJlYWtvdXQiOltbImRhdGV0aW1lLWZpZWxkIixbImZpZWxkLWlkIiwxMTcwMV0sImRheSJdXX0sImRhdGFiYXNlIjo5fSwiZGlzcGxheSI6ImxpbmUiLCJ2aXN1YWxpemF0aW9uX3NldHRpbmdzIjp7ImdyYXBoLmxhYmVsX3ZhbHVlX2ZyZXF1ZW5jeSI6ImFsbCJ9fQ==";	
	String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6MTA2MSwiZmlsdGVyIjpbImFuZCIsWyI9IixbImZpZWxkLWlkIiwxMTcxNV0sIkRJU0JVUlNFRCJdLFsiPSIsWyJmaWVsZC1pZCIsMTE3MTFdLCIxIl0sWyI-IixbImZpZWxkLWlkIiwxMTcwM10sIjIwMjItMDQtMTAiXV0sImFnZ3JlZ2F0aW9uIjpbWyJjb3VudCJdXSwiYnJlYWtvdXQiOltbImRhdGV0aW1lLWZpZWxkIixbImZpZWxkLWlkIiwxMTcwMV0sImRheSJdXSwib3JkZXItYnkiOltbImRlc2MiLFsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDExNzAxXSwiZGF5Il1dXX0sImRhdGFiYXNlIjo5fSwiZGlzcGxheSI6InRhYmxlIiwidmlzdWFsaXphdGlvbl9zZXR0aW5ncyI6eyJ0YWJsZS5waXZvdCI6ZmFsc2V9fQ==";
	public DisbursedLoanCount(WebDriver driver) throws InterruptedException, IOException
	{
		driver.get(Url);
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[2]/div/div[2]/div/div[1]")).click();
		Thread.sleep(2000);
		try {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement Element=driver.findElement(By.xpath("//div[@text='March 14, 2022']"));
		 ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", Element);
		 Thread.sleep(3000);
		} catch(Exception e) {}
		
//		try {
//	        JavascriptExecutor js = (JavascriptExecutor) driver;                
//			WebElement Element=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[60]"));
//	        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", Element);
//	        Thread.sleep(3000);
//		} catch(Exception e) {}
//	        try {
//		        JavascriptExecutor js1 = (JavascriptExecutor) driver;                
//				WebElement Element1=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[39]"));
//		        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", Element1);
//		        Thread.sleep(3000);
//	        
//	        // js.executeScript("arguments[0].scrollIntoView();", Element);
//	} catch(Exception e) {}
//	        try {
//		        JavascriptExecutor js = (JavascriptExecutor) driver;                
//				WebElement Element=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[60]"));
//		        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", Element);
//		        Thread.sleep(3000);
//			} catch(Exception e) {}
//	        try {
//		        JavascriptExecutor js = (JavascriptExecutor) driver;                
//				WebElement Element=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[39]"));
//		        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", Element);
//		        Thread.sleep(3000);
//			} catch(Exception e) {}
	       
		try {
			
			SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
			Date date = new Date();
			String fileName = df.format(date);
			File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file,
					new File("./ImPlusLoanCount/" + "DisbursedLoanCount" + " " + "(" + " " + fileName + " " + ")" + ".jpg"));
		} catch (Exception e) {
		}   


		

	}
}

