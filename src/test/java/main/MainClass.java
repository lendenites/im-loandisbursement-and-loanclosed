package main;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import capabillity.Capability;
import implusloancount.ClosedLoanCount;
import implusloancount.DisbursedLoanCount;
import implusloancount.LogIn;

public class MainClass extends Capability
{
	WebDriver driver;
	@BeforeTest
	public void openchrome()
	{
		driver = WebCapability();
		driver.manage().window().maximize();

	}
	@Test(priority = 1)
	public void login() throws InterruptedException
	{
		new LogIn(driver);
	}

	@Test(priority = 2)
	public void loancount() throws InterruptedException, IOException
	{
		new DisbursedLoanCount(driver);
	}
	@Test(priority = 3)
	public void closed() throws InterruptedException, IOException
	{
		new ClosedLoanCount(driver);	
	}


	@AfterTest
	public void CloseBrowser()
	{
		driver.quit	();
	}
}
